package test.task1;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    private static final int SIZE_OF_ARRAY = Integer.MAX_VALUE - 3; // -Xmx13G
    private static final int RANDOM_NUMBER = (int) (Math.random() * SIZE_OF_ARRAY);

    public static void main(String[] args) {
        System.out.println("Задвоение произошло на числе: " + (RANDOM_NUMBER - 1));
        findRepeatingNumber();
        getThirdWordArray();
    }
    private static void findRepeatingNumber() {
        final int[] array = getFilledArray();
        for (int i = 0; i < array.length; i++) {
            if (array[i] != i && i == (RANDOM_NUMBER - 1)) {
                System.out.println("Отсутствующее число: " + i);
                return;
            }
        }
    }
    private static int[] getFilledArray() {
        final int[] array = new int[SIZE_OF_ARRAY + 1];
        Arrays.parallelSetAll(array, i -> i++);
        switch (RANDOM_NUMBER) {
            case 0: {
                array[RANDOM_NUMBER + 1] = array[RANDOM_NUMBER];
                break;
            }
            default:
                array[RANDOM_NUMBER - 1] = array[RANDOM_NUMBER];
        }
        return array;
    }
    private static void getThirdWordArray() {
        try {
            Pattern pattern = Pattern.compile("[a-zA-Zа-яА-Я]+");
            Matcher matcher = pattern.matcher(Arrays.toString(Files.lines(Paths.get
                            ("source.txt"), StandardCharsets.UTF_8).toArray(String[]::new)));
            System.out.println("\nКаждое третье слово в массиве: ");
            for (int i = 0; matcher.find(); i++)
                if (i % 3 == 0) System.out.print(matcher.group() + " ");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}